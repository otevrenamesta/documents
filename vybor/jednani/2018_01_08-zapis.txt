přítomni: Marcel, Dan, Láďa, Radomír
agenda:
1. hlasování per rollam
2. stav úkolů
* Marcel domluví s účetním použití tagů ve FlexiBee
* Marcel zřídí přístup Danovi do datové schránky
* Laďa zveřejní zápis z členské schůze
* zápis změny obsazení výboru do restříku
o připraví Dan, jakmile mu Marcel zřídí přístup do datovky
* Zbyněk do konce října předloží návrh nové funkcionality evidence smluv BISON, kterou dále budeme implementovat pomocí modulů
* pokračování Dana v Otevřených městech
o Marcel připraví smlouvu
* žádost o členství: Asociace pro elektromobilitu
o Marcel jim napíše mail, co od členství očekávají a jak mohou přispět k činnosti OM
3. CityVizor
* Brno
* Praha 5
* Praha 7
* programátor
4. výběr členských poplatků
5. rozpočet
6. členská schůze
7. projektová rada
8. různé
* IoT projekt | Lora | base48
* dotační tituly (dotaz na Radomíra)
* SLA
* Datovka - Dan
* schůzka BISON
* IM členů výboru
* jedeme na novém virtuálu
* členství Nadace Charty 77 | Petr Hazuza
* avízo nějakého pomocníka ICT

zápis:
1. hlasování per rollam
2. stav úkolů
* Laďa zveřejní zápis z členské schůze
o AI: bude zveřejněno
* Flexibee
o Zbyněk zjistil, že rozdělit platbu na části není problém, musí se definovat ty střediska
o domluveno, přikročíme k implementaci
o Marcel napíše účetnímu
* zápis změny obsazení výboru do restříku
o připraví Laďa
* budoucnost s vpsFree - https://pad.openalt.org/om_vpsfree
o Láďa sbírá informace
* výběrové řízení na pozici programátora pro CityVizor
* Marcel zřídí přístup Danovi do datové schránky
o Danovi zatím dopis nepřišel, bude o vývoji informovat
* zápis změny obsazení výboru do restříku
o AI: připraví Laďa
* Konto Bariéry: žádost o členství
o AI: Projednáno, počkáme na doručení formální žádosti - Laďa zařídí
* Zbyněk do konce října předloží návrh nové funkcionality evidence smluv BISON, kterou dále budeme implementovat pomocí modulů
o Bude probráno ve středu na schůzi BISONu
o Zbyněk napíše mail do Výboru 
* open data - Brno (SLA) | katalog městských dat  https://kmd.otevrenamesta.cz/ a katalog otevřených dat https://ckan.otevrenamesta.cz/
o https://monitor.otevrenamesta.cz/  https://diskurz.otevrenamesta.cz/
o AI: Marcel se podívá na návrh smlouvy
* žádost o členství: Asociace pro elektromobilitu
o Marcel jim napíše mail, co od členství očekávají a jak mohou přispět k činnosti OM
3. Stav CityVizoru
o Brno: uvidíme po schůzce
o Brno-střed: uvidíme po schůzce
o Praha 5: Radomír projedná
o Praha 7: Řeší Ondra, Dan napíše
o Faktura Internet Stream: proplatí se
o Mariánské Lázně: Dan zkontaktuje
o programátor: zatím Martin Kopeček
* AI: Dan do 31.1.2018 připraví návrh výběrového řízení
4. zbývající body odloženy na další jednání

