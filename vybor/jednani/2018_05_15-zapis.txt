2018-05-15
p��tomni:�La�a, Ondra, Radom�r, Zbyn�k
agenda:
    � projekty/rozpo�et
    � orienta�n� sazebn�k
    � GDPR - je t�eba n�co sp�chat? (m�me mailing, m�me kontakty)
z�pis:
    � probr�ny jednotliv� body navr�en�ho rozpo�tu a pozm�n�ny n�kter� polo�ky
    � V�bor schvaluje rozpo�et pro rok 2018 ve zn�n� z 2018-05-15 20:00.
    � hlasov�n�
    � pro: Zbyn�k Grepl, Ladislav Ne�n�ra, Radom�r Palovsk�, Ond�ej Profant
    � proti: nikdo
    � zdr�el se: nikdo
    � probr�n orienta�n� zasebn�k typov�ch prac� (nap�. program�tor, tester, p�ekladatel, ..). �lenov� v�boru dopln� do tabulky sv� n�vrhy
    � �lensk� sch�ze 2018
    � Ondra urychl� p��pravu
    � do programu zahrneme sezn�men� s podporovan�mi projekty
    � v projektu spr�vy identit vyu�ijeme mo�nost konzultac� s N�rodn� technickou knihovnou
    � p�edb�n� term�n dal�� sch�ze - 2018-05-22

