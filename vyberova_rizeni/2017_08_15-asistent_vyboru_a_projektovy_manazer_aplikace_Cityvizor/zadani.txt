Date: Fri, 25 Aug 2017 09:04:00 +0200
From: Marcel Kolaja <marcel@kolaja.eu>
To: konference@otevrenamesta.cz
Subject: výběrové řízení na asistenta výboru a projektového manažera aplikace Cityvizor
User-Agent: NeoMutt/20170113 (1.7.2)

Vážení příznivci otevřenosti,

minulý týden jsme vypsali ${SUBJ}:

http://www.otevrenamesta.cz/vyberove_rizeni_na_asistenta_Vyboru_a_projektoveho_manazera_aplikace_Cityvizor.html

Pro rozšíření okruhu oslovených posílám tuto informaci ještě do
konference. Pokud by měl někdo zájem a pouze by nestíhal termín uzávěrky,
ať nám dá vědět na vybor@otevrenamesta.cz a můžeme zvážit prodloužení
termínu.


S pozdravem

Marcel Kolaja                                        http://www.kolaja.eu/
--------------------------------------------------------------------------
"I could be wrong, of course. But I'm never wrong."
       -- Linus Torvalds
